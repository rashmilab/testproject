# TestProject
This repository contains tests to validate the Technical test form with various inputs
Selenium BDD cucumber framework is used for automation

# SYSTEM REQUIREMENTS
 JDK All version
 Chrome Browser
 IntelliJ (recommended)

## Getting started
clone the repository: gitclone https://gitlab.com/rashmi.gitlab/testproject.git

# Project structure
testproject
    src
        main
            java
                org
                    pages
                        technicalTestForm.java
        test
            java
                org
                    opencart
                        pages
                            stepdefs
            resources
                data    
                    TestDataBase.xlsx
                    uploadDoc.pdf
                features
                    TechnicalTestForm.feature
            extent.properties
            spark-config.xml
    pom.xml
    testng.xml



# HOW TO USE
1. Run Cucumber TestRunner from **src/test/java/runner**
2. Run Feature file (src/test/resources/features/)
3. Run Feature from Maven pom.xml file (mvn clean test)

# View Test results
After running the tests, view the test results under
1. html report: Under /target/cucumber-reports
2. **Extent** pdf report under: /test output/pdfReport
3. **SPARK Extent** /test output/SparkReport. 



#Additional information
cd existing_repo
git remote add origin https://gitlab.com/rashmi.gitlab/testproject.git
git branch -M main
git push -uf origin main
```
