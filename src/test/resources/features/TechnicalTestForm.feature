Feature: Technical Test form

  As a tester of the Technical form
  I want to be able to enter all details
  So that I can submit my form successfully

 // Background:

  //  Given I am on Testform login page

  Scenario: Successful Submission of the Technical Webpage
    Given I am on Testform login page
    And I fill the form with all the fields
    When I click on the Submit button
    Then I should see the Success message


  Scenario: Successful Form Submission with ONLY mandatory fields
    Given I am on Testform login page
    And I fill the form with only mandatory fields
    When I click on the Submit button
    Then I should see the Success message


  Scenario: Click Submit without entering any fields
    Given I am on Testform login page
    When I click on the Submit button
    Then I should see error message for all the mandatory fields


  Scenario: Invalid email format error test
    Given I am on Testform login page
    And I fill the form with invalid email format
    When I click on the Submit button
    Then I should see error message for email "Please enter a valid email"


  Scenario: Invalid MobileNumber format error test
    Given I am on Testform login page
    And I fill the form with invalid mobile number
    When I click on the Submit button
    Then I should see error message for mobile field "Please enter a valid mobile number"


  Scenario: Invalid Date format error test
    Given I am on Testform login page
    And I fill the form with a future date
    When I click on the Submit button
    Then I should see error message for Date field "Only dates today or before are allowed"






#  Scenario Outline: Unsuccessful login with invalid or empty credentials
#    Given I have entered invalid "<username>" and "<password>"
#    When I click on the login button
#    Then I should see an error message indicating "<error_message>"
#
#
#    Examples:
#      | username          | password        | error_message     |  |
#      | invalid@gmail.com | invalidPassword | Warning: No match |  |
#      | abcccc            | validPassword   | Warning: No match |  |
#      | valid@email.com   | abccc           | Warning: No match |  |
