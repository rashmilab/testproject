package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = "src/test/resources/features", // Path to your feature files
        glue = "org.opencart.pages.stepdefs", // Package name where your step definitions are located
        //tags = "@smokeTest", // Optional: Tags to filter the scenarios to run
        plugin = {"pretty", "html:target/cucumber-reports", "json:target/cucumber.json",
        "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
        "timeline:test-output-thread/"} // Optional: Plugins for reporting
)
public class TestRunner extends AbstractTestNGCucumberTests {

//    @Override
//    @DataProvider(parallel = true)
//    public Object[][] scenarios() {
//        return super. scenarios();
//    }
}


