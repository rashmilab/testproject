package org.opencart.pages.stepdefs;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.pages.technicalTestForm;
import org.testng.Assert;
import utils.formDetailsMap;
import utils.ExcelReader;

import java.io.IOException;
import java.time.Duration;
import java.util.Map;


public class TestFromStepDef {

    private WebDriver driver;
    private org.pages.technicalTestForm technicalTestForm;
    String attachmentFilePath = System.getProperty("user.dir")+"/src/test/resources/data/uploadDoc.pdf";

    @Before
    public void setup(){
        driver = new ChromeDriver();
    }

    @After
    public void tearDown(){
        if(driver!=null){
            driver.quit();
        }
    }

    @Given("I am on Testform login page")
    public void i_am_on_testform_login_page() {
        driver.get("https://go.gov.sg/gt-qe");
        technicalTestForm = new technicalTestForm(driver);
        //check for best practices to add wait
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @When("I click on the Submit button")
    public void IclickontheSubmitbutton() throws InterruptedException {
        technicalTestForm.clickSubmit();
    }

    @Then("I should see the Success message")
    public void IshouldseetheSuccessmessage() {
        Assert.assertTrue(technicalTestForm.checkSuccessMsg());
    }

    @Then("I should see error message for all the mandatory fields")
    public void iShouldSeeErrorMessageForAllTheMandatoryFields() throws InterruptedException {
        int numOfError = technicalTestForm.checkformandatoryfields();
        System.out.println(numOfError);
        Assert.assertEquals(numOfError, 7, "Expected 7 error icons, but found " + numOfError);
    }

    @Then("I should see error message for email {string}")
    public void iShouldSeeErrorMessageForEmail(String emailError) throws InterruptedException {
        String actualEmailError = technicalTestForm.checkEmailFormat();
        System.out.println("ActualErrorText: " + actualEmailError);
        Assert.assertEquals(actualEmailError, emailError);
    }

    @Then("I should see error message for mobile field {string}")
    public void iShouldSeeErrorMessageForMobileField(String mobileError) throws InterruptedException {
        String actualMobileError = technicalTestForm.checkMobileFormat();
        System.out.println("ActualErrorText: " + actualMobileError);
        Assert.assertEquals(actualMobileError, mobileError);
    }

    @Then("I should see error message for Date field {string}")
    public void iShouldSeeErrorMessageForDateField(String dateError) throws InterruptedException {
        String actualDateError = technicalTestForm.checkDateFormat();
        System.out.println("ActualErrorText: " + actualDateError);
        Assert.assertEquals(actualDateError, dateError);
    }

    @And("I fill the form with all the fields")
    public void iFillTheFormWithalltheFields() throws IOException, InterruptedException {
        String excelFilePath = "src/test/resources/data/TestDataBase.xlsx";
        String desiredTestCaseID = "TC001";
        fillFormWithDataFromXLS(excelFilePath,desiredTestCaseID);
        technicalTestForm.clickChooseFile(attachmentFilePath);

    }

    @And("I fill the form with only mandatory fields")
    public void iFillTheFormWithOnlyMandatoryFields() throws IOException, InterruptedException {
        String excelFilePath = "src/test/resources/data/TestDataBase.xlsx";
        String desiredTestCaseID = "TC002";
        fillFormWithDataFromXLS(excelFilePath,desiredTestCaseID);
     }

    @And("I fill the form with invalid email format")
    public void iFillTheFormWithInvalidEmailFormat() throws IOException, InterruptedException {
        String excelFilePath = "src/test/resources/data/TestDataBase.xlsx";
        String desiredTestCaseID = "TC003";
        fillFormWithDataFromXLS(excelFilePath,desiredTestCaseID);
    }

    @And("I fill the form with invalid mobile number")
    public void iFillTheFormWithInvalidMobileNumber() throws IOException, InterruptedException {
        String excelFilePath = "src/test/resources/data/TestDataBase.xlsx";
        String desiredTestCaseID = "TC004";
        fillFormWithDataFromXLS(excelFilePath,desiredTestCaseID);
    }

    @And("I fill the form with a future date")
    public void iFillTheFormWithaFutureDate() throws IOException, InterruptedException {
        String excelFilePath = "src/test/resources/data/TestDataBase.xlsx";
        String desiredTestCaseID = "TC005";
        fillFormWithDataFromXLS(excelFilePath,desiredTestCaseID);
    }


   private void fillFormWithDataFromXLS (String excelFilePath, String testType) throws IOException, InterruptedException {
       Map<String, formDetailsMap> employeeMap = ExcelReader.readExcelFile(excelFilePath);
       formDetailsMap employee = employeeMap.get(testType);
       if (employee != null) {
           System.out.println("Employee details for Test Case ID " + testType + ":");

           System.out.println("First Name: " + employee.getFirstName());
           technicalTestForm.enterFname(employee.getFirstName());

           System.out.println("Last Name: " + employee.getLastName());
           technicalTestForm.enterLname(employee.getLastName());

           System.out.println("Email: " + employee.getEmailId());
           technicalTestForm.enterEmail(employee.getEmailId());

           System.out.println("Gender: " + employee.getGender());
           technicalTestForm.selectRadioBtn(employee.getGender());

           System.out.println("MobileNumber: " + employee.getMobileNumber());
           technicalTestForm.enterMobileNum(employee.getMobileNumber());

           System.out.println("DOB: " + employee.getDOB());
           technicalTestForm.enterDOB(String.valueOf(employee.getDOB()));

           System.out.println("Hobbies: " + employee.getHobbies());
           technicalTestForm.selectCheckbox(employee.getHobbies());

           System.out.println("Address: " + employee.getAddress());
           technicalTestForm.enterAddress(employee.getAddress());


       } else {
           System.out.println("Test Case ID " + testType + " not found.");
       }
   }

}

