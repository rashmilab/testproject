package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {

    public static Map<String, formDetailsMap> readExcelFile(String filePath) {
        Map<String, formDetailsMap> detailsMapHashMap = new HashMap<>();
       // SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try (FileInputStream fis = new FileInputStream(new File(filePath));
             Workbook workbook = new XSSFWorkbook(fis)) {

            Sheet sheet = workbook.getSheetAt(0); // Assumes the first sheet

            for (Row row : sheet) {
                if (row.getRowNum() == 0) continue; // Skip the header row

                formDetailsMap userInputs = new formDetailsMap();
                userInputs.setTestCaseId(row.getCell(0).getStringCellValue());
                userInputs.setFirstName(row.getCell(1).getStringCellValue());
                userInputs.setLastName(row.getCell(2).getStringCellValue());
                userInputs.setEmailId(row.getCell(3).getStringCellValue());
                userInputs.setGender(row.getCell(4).getStringCellValue());

                //userInputs.setMobileNumber(row.getCell(5).getStringCellValue());
                Cell MobNum = row.getCell(5);
                if (MobNum != null) {
                    if (MobNum.getCellType() == CellType.NUMERIC) {
                        // Read numeric cell and convert to string
                        userInputs.setMobileNumber(String.valueOf((long) MobNum.getNumericCellValue()));
                    } else if (MobNum.getCellType() == CellType.STRING) {
                        // Read string cell directly
                        userInputs.setMobileNumber(MobNum.getStringCellValue());
                    }
                }

                // userInputs.setDOB(String.valueOf(row.getCell(6).getDateCellValue()));
                Cell cell = row.getCell(6);
                Date date;
                if (cell.getCellType() == CellType.NUMERIC) {
                    if (DateUtil.isCellDateFormatted(cell)) {
                        date = cell.getDateCellValue();
                    } else {
                        throw new IllegalArgumentException("Numeric cell is not formatted as a date.");
                    }
                } else if (cell.getCellType() == CellType.STRING) {
                    String dateStr = cell.getStringCellValue();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    date = dateFormat.parse(dateStr);
                } else {
                    throw new IllegalArgumentException("Unsupported cell type for date value.");
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                userInputs.setDOB(dateFormat.format(date));

                userInputs.setHobbies(row.getCell(7).getStringCellValue());
//                userInputs.setAttachment(row.getCell(8).getStringCellValue());
//                userInputs.setLocation(row.getCell(9).getStringCellValue());
                userInputs.setAddress(row.getCell(10).getStringCellValue());


                detailsMapHashMap.put(userInputs.getTestCaseId(), userInputs);
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return detailsMapHashMap;
    }

    // method to run excel reader locally
    public static void main(String[] args) {
        String excelFilePath = "src/test/resources/data/TestDataBase.xlsx";
        Map<String, formDetailsMap> employeeMap = readExcelFile(excelFilePath);
    
        String desiredTestCaseID = "TC002"; // Replace "TC_002" with the desired test case ID
    
        // Retrieve the Employee object for the desired test case ID
        formDetailsMap employee = employeeMap.get(desiredTestCaseID);
    
        if (employee != null) {
            System.out.println("Employee details for Test Case ID " + desiredTestCaseID + ":");
            System.out.println("First Name: " + employee.getFirstName());
            System.out.println("Last Name: " + employee.getLastName());
            System.out.println("DOB: " + employee.getDOB());
            System.out.println("Email ID: " + employee.getEmailId());
            System.out.println("Gender: " + employee.getGender());
        } else {
            System.out.println("Test Case ID " + desiredTestCaseID + " not found.");
        }
    }
    
}
