package utils;

public class formDetailsMap {
    private String DOB ;
    private String testCaseId;
    private String firstName;
    private String lastName;
    private int age;
    private String emailId;
    private String gender;
    private String MobileNumber;
    private String Address;
    private String Hobbies;
    private String Location;
    private String Attachment;

    // Getters and setters
    public String getTestCaseId() {
        return testCaseId;
    }
    public void setTestCaseId(String testCaseId) {this.testCaseId = testCaseId;}

    public String getFirstName() {return firstName;}
    public void setFirstName(String firstName) {this.firstName = firstName;}

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {return MobileNumber;}
    public void setMobileNumber(String MobileNumber) {this.MobileNumber = MobileNumber;}

    public String getDOB() {return DOB;}
    public void setDOB(String DOB) {this.DOB = DOB;}

    public String getHobbies() {return Hobbies;}
    public void setHobbies(String Hobbies) {this.Hobbies = Hobbies;}

    public String getLocation() {return Location;}
    public void setLocation(String Location) {this.Location = Location;}

    public String getAddress() {return Address;}
    public void setAddress(String Address) {this.Address = Address;}


    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "TestCaseID='" + testCaseId + '\'' +
                ", FirstName='" + firstName + '\'' +
                ", LastName='" + lastName + '\'' +
                ", Email='" + emailId + '\'' +
                ", Gender='" + gender + '\'' +
                ", MobileNumber='" + MobileNumber + '\'' +
                ", DOB=" + DOB + + '\'' +
                ", Hobbies='" + Hobbies + '\'' +
                ", Attachment='" + Attachment + '\'' +
                ", Location='" + Location + '\'' +
                ", Address='" + Address + '\''+
                '}';
    }
}
