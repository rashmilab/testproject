package org.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class technicalTestForm {

    private WebDriver driver;
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    private By fNameInputLocator = By.xpath("//input[@aria-label='1. First Name']");
    private By lNameInputLocator = By.xpath("//input[@aria-label='2. Last Name']");
    private By emailInputLocator = By.xpath("//input[@autocomplete='email']");


    private By mobileNumLocator = By.xpath("//input[@autocomplete='tel']");
    private By dobLocator = By.xpath("//input[@placeholder='dd/mm/yyyy']");



    private By addressLocator = By.xpath("//textarea[@aria-label='10. Address']");
    private By chooseFileLocator = By.xpath("//input[@type='file']");
    private By submitBtnLocator = By.xpath("//button [@type='button' and @class='chakra-button css-1szjd8b']");
    private By successMsgLocator = By.xpath("//h2[normalize-space()='Thank you for filling out the form.']");

    /* Error message locators */
    private By mandatoryErrorMsgLocator = By.xpath("//div[@class='chakra-form__error-message css-1vo62p7']");
    private By emailFormatErrorLocator = By.xpath("//div[contains(text(),'Please enter a valid email')]");
    private By mobileFormatErrorLocator = By.xpath("//div[contains(text(),'Please enter a valid mobile number')]");
    private By dateFormatErrorLocator = By.xpath("//div[contains(text(),'Only dates today or before are allowed')]");

    // Constructor
    public technicalTestForm(WebDriver driver) {
        this.driver = driver;
    }


    // Methods
    public void enterFname(String Fname) {
        WebElement fNameInput = driver.findElement (fNameInputLocator);
        fNameInput.sendKeys(Fname);
    }

    public void enterLname(String Lname) {
        WebElement lnameInput = driver.findElement (lNameInputLocator);
        lnameInput.sendKeys(Lname);
    }
    public void enterEmail(String Email) {
        WebElement emailInput = driver.findElement (emailInputLocator);
        emailInput.sendKeys(Email);
    }
    public void enterMobileNum(String MobileNum) {
        WebElement mobileNumber = driver.findElement(mobileNumLocator);
        mobileNumber.sendKeys(MobileNum);
    }

    public void enterDOB(String DOB) {
        WebElement DOBInput = driver.findElement(dobLocator);
        DOBInput.click();
        DOBInput.sendKeys(DOB);
    }

    public void selectRadioBtn(String radioBtnText) {
        By radioBtnLocator = By.xpath("//span[contains(text(),'" + radioBtnText + "')]/preceding-sibling::span[contains(@class, 'chakra-radio__control')]");
        WebElement radioBTnInput = driver.findElement(radioBtnLocator);
        WebElement checkbox = wait.until(ExpectedConditions.elementToBeClickable(radioBTnInput));
        checkbox.click();
    }


    public void selectCheckbox(String checkBoxText) {
    // ** Check This!!***** //
    By checkboxLocator = By.xpath("//span[contains(text(),'" + checkBoxText + "')]/preceding-sibling::span[contains(@class, 'chakra-checkbox__control')]");
    WebElement checkBoxInput = driver.findElement(checkboxLocator);
    // Wait for the element to be clickable and click it
    WebElement checkbox = wait.until(ExpectedConditions.elementToBeClickable(checkBoxInput));
    checkbox.click();
    }

    public void clickChooseFile (String filepath) {
        WebElement ChooseFile = driver.findElement (chooseFileLocator);
        //ChooseFile.click();
        ChooseFile.sendKeys(filepath);
//        By fileExtensionLocator = By.xpath("//p[contains(@class, 'chakra-text') and contains(text(), '.pdf')]");
//
//        WebElement fileExtnVisibility = driver.findElement(fileExtensionLocator);
//        WebElement fileNameExtension = wait.until(ExpectedConditions.visibilityOfElementLocated((By) fileExtnVisibility));
//        WebElement fileNameElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(@class, 'chakra-text') and contains(text(), 'Requirements')]")));
//        String fileName = fileNameExtension.getText();
//        if (!fileName.endsWith(".pdf")) {
//            throw new AssertionError("Uploaded file is not a PDF. File uploaded: " + fileName);
//        }
    }


    public void enterAddress(String address) {
        WebElement AddressInput = driver.findElement(addressLocator);
        AddressInput.sendKeys(address);
    }

    public void clickSubmit() throws InterruptedException {
        WebElement SubmitBtn = driver.findElement (submitBtnLocator);
        Thread.sleep(2000);
        SubmitBtn.click();
    }

    public boolean checkSuccessMsg() {
        return driver.findElement(successMsgLocator).isDisplayed();
    }

    public int checkformandatoryfields () throws InterruptedException {
        List<WebElement> errorIcons = driver.findElements (mandatoryErrorMsgLocator);
        return errorIcons.size();
    }

    public String checkEmailFormat () throws InterruptedException {
        WebElement EmailFormatErr = driver.findElement(emailFormatErrorLocator);
        String emailFormatErr = EmailFormatErr.getText();
        return emailFormatErr;
    }

    public String checkMobileFormat () throws InterruptedException {
        WebElement mobileFormatErr = driver.findElement(mobileFormatErrorLocator);
        return mobileFormatErr.getText();
    }

    public String checkDateFormat () throws InterruptedException {
        WebElement dateFormatErr = driver.findElement(dateFormatErrorLocator);
        return dateFormatErr.getText();
    }

}
